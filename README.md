# pkg-update
## Goals of this script/project:
 * Make it easy to update userland packages of jails
 * To get an out of the box experience, so no external dependencies required
 * For me to learn more about scripting

## Requirements:
 - FreeBSD
 - Jails to update

## Installation:
- Download pkg-update script to folder, example:
`/usr/local/bin`
- Set permissions:
`chmod +x pkg-update`
- Create log directory:
`mkdir /var/log/pkg-update`
- Change personal settings on top of script
`vim /usr/local/bin/pkg-update`
- Reload internal hash table
`rehash`

## Usage
`pkg-update <jailname/jail#> silent (optional)`

## Current TODO's:
 - Feature: Restart jail if required
 - Feature: Implement jail management support if available (like ezjail and iocage)
 - Feature: Add "all" argument to update all jails
 - Improvement: Check if updates are available before continuing
 - Improvement: Maybe learn better code standard
